function add(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2 );
	console.log(num1+num2);
}

function subtract(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2 );
	console.log(num1 - num2);
}

add(5,15);
subtract(20,5);

function multiply(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
}

function divide(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}

let product = multiply(50,10);
console.log(product);
let quotient = divide(50,10);
console.log(quotient);

function area(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:")
	return 3.1416 * radius ** 2
}

let circleArea = area(15);
console.log(circleArea);

function average(num1, num2, num3, num4){
	console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	return (num1+num2+num3+num4)/4;
}

let averageVar = average(20,40,60,80);
console.log(averageVar);

function checkIfPass(yourScore, totalScore){
	let percentage = (yourScore / totalScore) * 100
	let isPassed = percentage >= 75;
	console.log("Is " + yourScore + "/" + totalScore + " a passing score?");
	return isPassed;
}

let isPassingScore = checkIfPass(38,50);
console.log(isPassingScore);

